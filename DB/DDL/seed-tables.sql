
/*==============================================================*/
/* Table: Employee_group                                        */
/*==============================================================*/
create table Customer_group (
   customer_group_id            serial,
   group_title            varchar(100), 
   primary key (customer_group_id)
); 

/*==============================================================*/
/* Table: Course_type                                           */
/*==============================================================*/
create table Course_type (
   course_type_id       serial,
   title_type           varchar(100),
   primary key (course_type_id)
);

/*==============================================================*/
/* Table: Course                                                */
/*==============================================================*/
create table Course (
   course_id            serial,
   type_id              integer not null,
   title                varchar(100),
   price                decimal(8,2),
   is_active            boolean,
   created_date  timestamp,
   modified_date timestamp,
   course_type_id       integer not null,
   primary key (course_id)
);

/*==============================================================*/
/* Table: Customer                                              */
/*==============================================================*/
create table Customer (
   customer_id            serial,
   first_name             varchar(100),
   last_name              varchar(100),
   login                  varchar(100),
   password               varchar(100),
   is_active              boolean,
   created_date  timestamp,
   modified_date timestamp,
   customer_group_id            integer not null,
   primary key (customer_id)
);


/*==============================================================*/
/* Table: Order_course                                               */
/*==============================================================*/
create table Order_course (
   order_id            serial,
   portion             integer not null,
   date_o              date,
   summ                decimal(8,2),
   created_date  timestamp,
   modified_date timestamp,
   course_id           integer not null,
   customer_id         integer not null,
   primary key (order_id)
);

/*==============================================================*/
/* Table: Availability                                          */
/*==============================================================*/
create table Availability (
   availability_id           serial,
   mon_a                boolean,
   tue_a                boolean,
   wed_a                boolean,
   tru_a                boolean,
   fri_a                boolean,
   sat_a                boolean,
   sun_a                boolean,
   course_id            integer not null,
   primary key (availability_id)
);

alter table Availability
   add constraint FK_AVAILABI_INCLUDES2_COURSE foreign key (course_id)
      references Course (course_id)
      on delete restrict on update restrict;

alter table Course
   add constraint FK_COURSE_HAS_COURSE_T foreign key (course_type_id)
      references Course_type (course_type_id)
      on delete restrict on update restrict;

alter table Customer
   add constraint FK_CUSTOMER_INCLUDED__CUSTOMER foreign key (customer_group_id)
      references Customer_group (Customer_group_id)
      on delete restrict on update restrict;

alter table Order_course
   add constraint FK_ORDER_DEPENDS_O_AVAILABI foreign key (order_id)
      references Availability (course_id)
      on delete restrict on update restrict;

alter table Order_course
   add constraint FK_ORDER_MAKES_CUSTOMER foreign key (customer_id)
      references Customer (customer_id)
      on delete restrict on update restrict;
