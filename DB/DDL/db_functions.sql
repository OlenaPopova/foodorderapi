/*FUNCTION fn_add_customer_group*/

CREATE OR REPLACE FUNCTION fn_add_customer_group(
    group_title_arg VARCHAR(100))
    RETURNS integer
    AS $$

DECLARE
  rcount INTEGER DEFAULT 0;
BEGIN
      
IF NOT EXISTS(SELECT  group_title 
      FROM   customer_group
      WHERE  group_title = group_title_arg) then
INSERT INTO Customer_group(group_title) VALUES (group_title_arg);
else
RETURN -1;
end if;
  
RETURN rcount;
END;

$$
LANGUAGE plpgsql;

/*FUNCTION fn_add_customer*/

CREATE OR REPLACE FUNCTION fn_add_customer(
	first_name_arg VARCHAR(100),
	last_name_arg VARCHAR(100),
	login_arg VARCHAR(100),
	password_arg VARCHAR(100),
	is_active_arg boolean,
	customer_group_id_arg integer)
    RETURNS integer
    AS $$

DECLARE
  rcount INTEGER DEFAULT 0;
BEGIN
IF NOT EXISTS(SELECT  login_arg
      FROM   customer
      WHERE  login = login_arg) then
INSERT INTO Customer(first_name,last_name,login,pass,is_active,
   customer_group_id) VALUES (first_name_arg,last_name_arg,login_arg,password_arg,is_active_arg,
   customer_group_id_arg);
else
RETURN -1;
end if;
  
RETURN rcount;
END;

$$
LANGUAGE plpgsql;

/*FUNCTION fn_add_course_type*/

CREATE OR REPLACE FUNCTION fn_add_course_type(
    title_type_arg VARCHAR(100))
    RETURNS integer
    AS $$

DECLARE
  rcount INTEGER DEFAULT 0;
BEGIN
      
IF NOT EXISTS(SELECT  title_type 
      FROM   course_type
      WHERE  title_type = title_type_arg) then
INSERT INTO Course_type(title_type) VALUES (title_type_arg);
else
RETURN -1;
end if;
  
RETURN rcount;
END;

$$
LANGUAGE plpgsql;

/*FUNCTION fn_add_course*/

CREATE OR REPLACE FUNCTION fn_add_course(
	title_arg VARCHAR(100),
	price_arg decimal,
	is_active_arg boolean,
	course_type_id_arg integer)
    RETURNS integer
    AS $$

DECLARE
  rcount INTEGER DEFAULT 0;
BEGIN
      
IF NOT EXISTS(SELECT  title 
      FROM   course
      WHERE  title = title_arg) then
INSERT INTO Course(title, price, is_active, course_type_id) VALUES (title_arg, price_arg, is_active_arg, course_type_id_arg);
else
RETURN -1;
end if;
  
RETURN rcount;
END;

$$
LANGUAGE plpgsql;

/*FUNCTION fn_add_availability*/

CREATE OR REPLACE FUNCTION fn_add_availability(
	mon_a_arg boolean,
	tue_a_arg boolean,
	wed_a_arg boolean,
	thu_a_arg boolean,
	fri_a_arg boolean,
	sat_a_arg boolean,
	sun_a_arg boolean,
	course_id_arg integer)
    RETURNS integer
    AS $$

DECLARE
  rcount INTEGER DEFAULT 0;
BEGIN
      
IF NOT EXISTS(SELECT  course_id 
      FROM   availability
      WHERE  course_id = course_id_arg) then
INSERT INTO Availability(mon_a, tue_a,wed_a,thu_a,fri_a,sat_a,sun_a,course_id) VALUES (mon_a_arg, tue_a_arg,wed_a_arg,thu_a_arg,fri_a_arg,sat_a_arg,sun_a_arg,course_id_arg);
else
RETURN -1;
end if;
  
RETURN rcount;
END;

$$
LANGUAGE plpgsql;

/*FUNCTION fn_add_order_course*/

CREATE OR REPLACE FUNCTION fn_add_order_course(
	portions_arg integer,
	date_o_arg date,
	summ_arg decimal,
	course_id_arg integer,
	customer_id_arg integer)
    RETURNS integer
    AS $$

DECLARE
  rcount INTEGER DEFAULT 0;
BEGIN
      
IF NOT EXISTS(SELECT  customer_id 
      FROM   order_course
      WHERE  customer_id_arg = customer_id) then
INSERT INTO Order_course(portions, date_o, summ, course_id, customer_id) VALUES (portions_arg, date_o_arg, summ_arg, course_id_arg, customer_id_arg);
else
RETURN -1;
end if;
  
RETURN rcount;
END;
$$
LANGUAGE plpgsql;