/*==============================================================*/
/* Table: Employee_group                                        */
/*==============================================================*/
CREATE TABLE Customer_group (
   customer_group_id            SERIAL,
   group_title            VARCHAR(100) NOT NULL, 
created_date  TIMESTAMP,
   PRIMARY KEY (customer_group_id)
); 

/*==============================================================*/
/* Table: Course_type                                           */
/*==============================================================*/
CREATE TABLE Course_type (
   course_type_id       SERIAL,
   title_type           VARCHAR(100) NOT NULL,
   modified_date TIMESTAMP,
   created_date  TIMESTAMP,
   PRIMARY KEY (course_type_id)
);

/*==============================================================*/
/* Table: Course                                                */
/*==============================================================*/
CREATE TABLE Course (
   course_id            SERIAL,
   title                VARCHAR(100) NOT NULL,
   price                DECIMAL(8,2) NOT NULL,
   is_active            BOOLEAN NOT NULL,
   created_date  TIMESTAMP,
   modified_date TIMESTAMP,
   course_type_id       INTEGER NOT NULL,
   PRIMARY KEY (course_id)
);

/*==============================================================*/
/* Table: Customer                                              */
/*==============================================================*/
CREATE TABLE Customer (
   customer_id            SERIAL,
   first_name             VARCHAR(100) NOT NULL,
   last_name             VARCHAR(100)NOT NULL,
   login                  VARCHAR(100) NOT NULL,
   pass               VARCHAR(100) NOT NULL,
   is_active              BOOLEAN NOT NULL,
   created_date  TIMESTAMP,
   modified_date TIMESTAMP,
   customer_group_id            INTEGER NOT NULL,
   PRIMARY KEY (customer_id)
);


/*==============================================================*/
/* Table: Order_course                                               */
/*==============================================================*/
CREATE TABLE Order_course (
   order_id            SERIAL,
   portions             INTEGER NOT NULL,
   date_o              DATE NOT NULL,
   summ                DECIMAL(8,2) NOT NULL,
   created_date  TIMESTAMP,
   modified_date TIMESTAMP,
   course_id           INTEGER NOT NULL,
   customer_id         INTEGER NOT NULL,
  PRIMARY KEY (order_id)
);

/*==============================================================*/
/* Table: Availability                                          */
/*==============================================================*/
CREATE TABLE Availability (
   availability_id           SERIAL,
   mon_a                BOOLEAN NOT NULL,
   tue_a                BOOLEAN NOT NULL,
   wed_a                BOOLEAN NOT NULL,
   thu_a                BOOLEAN NOT NULL,
   fri_a               BOOLEAN NOT NULL,
   sat_a                BOOLEAN NOT NULL,
   sun_a                BOOLEAN NOT NULL,
   course_id            INTEGER NOT NULL,
   PRIMARY KEY (availability_id)
);

ALTER TABLE Availability
   ADD CONSTRAINT FK_AVAILABI_INCLUDES2_COURSE FOREIGN KEY (course_id)
      REFERENCES Course (course_id)
      ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Course
   ADD CONSTRAINT FK_COURSE_HAS_COURSE_T FOREIGN KEY (course_type_id)
      REFERENCES Course_type (course_type_id)
     ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Customer
   ADD CONSTRAINT FK_CUSTOMER_INCLUDED__CUSTOMER FOREIGN KEY (customer_group_id)
      REFERENCES Customer_group (Customer_group_id)
      ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Order_course
   ADD CONSTRAINT FK_ORDER_DEPENDS_O_AVAILABI FOREIGN KEY (order_id)
      REFERENCES Availability (course_id)
      ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE Order_course
   ADD CONSTRAINT FK_ORDER_MAKES_CUSTOMER FOREIGN KEY (customer_id)
      REFERENCES Customer (customer_id)
      ON DELETE RESTRICT ON UPDATE RESTRICT;